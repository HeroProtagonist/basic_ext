// https://github.com/waveshare/e-Paper
// https://github.com/speedyg0nz/MagInkCal/blob/main/display/display.py
// https://www.rubyguides.com/2018/03/write-ruby-c-extension/
// https://dev.to/vinistock/creating-ruby-native-extensions-kg1

#include "ruby.h"
#include "extconf.h"


VALUE rb_return_nil() {
  return Qnil;
}

VALUE rb_print_hello() {
  return rb_sprintf("Hello");
}

void Init_foobar() {
  VALUE mod = rb_define_module("RubyGuides");

  rb_define_method(mod, "return_nil", rb_return_nil, 0);
  rb_define_method(mod, "print_hello", rb_print_hello, 0);
}
